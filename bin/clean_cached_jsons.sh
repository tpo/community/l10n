#!/bin/bash
# This small script is run from the root folder.
# it will remove the last page of the component information
# (the ones that have '"next": null' inside, and page in the filename)
# this way we can ask for this page again and get the new changes
# while we keep the cached previous pages, as they are not going to change
# (the queries are ordered by timestamp)

grep -lrIZ '\"next\": null' stats/comp*page*.json | xargs -0 rm --
grep -lrIZ '\"next\": null' stats/translations-*changes-*.json | xargs -0 rm --
rm public/pies/*.png
rm stats/comp*statistics*.json
