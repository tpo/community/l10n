#!/usr/bin/env python3
# this script is based/copied from
# Micah Lee's script for OnionShare
# translations at
# https://github.com/onionshare/onionshare/blob/main/docs/check-weblate.py
# modified by emmapeel for the Tor Project - 2023
import os.path
import json
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import datetime


dt = datetime.datetime.utcnow()
nice_date_built = dt.strftime("%A, %d %B, %Y")

def total_language_pie(lang, component_list, component_translations, minimum=10, img_filename='test'):
    # here we make a pie with information about translation status
    # for one language
    img_path = f'public/pies/{ img_filename }.png'
    if os.path.isfile(img_path):
        # we dont need to make it every day
        pass
    else:
        values_to_map = {
                    'failing': 'orange',
                    'approved': 'mediumpurple',
                    'translated': 'lightseagreen',
                    'untranslated':'palegoldenrod',
                    'fuzzy': 'grey',
                    'suggestions': 'lightblue'
                    }
        numbers = { v: 0 for v in values_to_map }

        if component_list == 'all':
            component_list = component_translations.keys()

        for one_component in component_list:
            comp_values = component_translations[one_component][lang]
            comp_values['translated'] = comp_values['translated'] - comp_values['approved']

            for onevalue in values_to_map.keys():
                try:
                    numbers[onevalue] += comp_values[onevalue]
                except KeyError:
                    pass
            # we need to bring back the number to its actual value, otherwise the stats go bezerk
            comp_values['translated'] =  comp_values['translated'] + comp_values['approved']

        fig, ax = plt.subplots(figsize=(1,1), dpi=50, constrained_layout=True)
        pie_worthy = {k: v for k, v in numbers.items() if v > minimum}
        colors = [ values_to_map[x] for x in pie_worthy.keys() ]
        ax.pie(pie_worthy.values(), colors=colors)

        try:
            plt.savefig(img_path, transparent=True)
        except FileNotFoundError:
            os.makedirs('public/pies')
        plt.close()


async def approved_translated_failing(all_languages):
    #the number of strings failing, translated or approved for
    # our most translated languages
    # general stats
    # is the image under the heading at https://tpo.pages.torproject.net/community/l10n/stats.html
    values_to_map = [ 'failing', 'translated', 'approved']
    minimum_translations = 2500
    width = 0.25  # the width of the bars
    pie_langs = [ x['code'] for x in all_languages.values() if x['translated'] > minimum_translations ]
    numbers = {}
    for v in values_to_map:
        numbers[v] = []
    for language in pie_langs:
        for onevalue in values_to_map:
            numbers[onevalue].append(all_languages[language][onevalue])

    bar_colors = ['orange', 'lightseagreen', 'purple']
    x = np.arange(len(pie_langs))  # the label locations
    width = 0.25  # the width of the bars
    multiplier = 0

    fig, ax = plt.subplots(layout='constrained', figsize=(18,5))
    for attribute, measurement in numbers.items():
        offset = width * multiplier
        rects = ax.bar(x + offset, measurement, width, label=attribute, color=bar_colors[multiplier])
        ax.bar_label(rects, padding=3)
        multiplier += 1

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Strings')
    ax.set_title(f'Failing, translated and approved strings in Tor Project\n{ nice_date_built }')
    ax.set_xlabel(f'(only languages with more than { minimum_translations } translated strings are shown)')
    ax.set_xticks(x + width, pie_langs)
    ax.legend(loc='upper left', ncols=3)
    ax.set_ylim(0, 10000)
    plt.savefig('public/approved_reviewed_per_lang_bars.png')

async def main(weblate_project='tor'):
    global languages

    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} API_KEY")
        print(
            "You can find your personal API key at: https://hosted.weblate.org/accounts/profile/#api"
        )
        return

    api_token = sys.argv[1]
    # Get the list of languages in the project
    res = await api(f"/api/projects/{ weblate_project }/languages/")
    for obj in res:
        if obj["code"] != "en":
            languages[obj["code"]] = obj
            try:
                languages[obj["code"]]["last_change"] = ":".join(
                    obj["last_change"].split(":")[:2]
                ).replace("T", " ")
            except AttributeError:
                languages[obj["code"]]["last_change"] = None



    await get_all_components()
    await approved_translated_failing(languages)
    #print(component_translations)
    await total_language_pie('id', ['tpo-web', 'tails-gui', 'onionsprouts-bot', 'community-portal'], component_translations )

    # start building the pages for each component and category
    # first gather some markdown files in bigger md files
    # for the TOC to work
    filenames = [
        "stats/pies.md",
    ]
    with open("stats/all-pies.md", "w") as outfile:
        for fname in filenames:
            with open(fname) as infile:
                for line in infile:
                    outfile.write(line)

    make_html("stats/all-pies.md", "public/admin-pies.html")
    #print(component_translations)

    #print(component_changes['Translation added'])
    #print(component_categories.items())
    #await get_general_week_activity()
    #print(component_translations['glossary'])

if __name__ == "__main__":
    asyncio.run(main())
