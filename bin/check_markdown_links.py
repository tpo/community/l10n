#!/usr/bin/env python3

# this script checks for the most common error in markdown translations:
# Links should be [word](link).
# written by emmapeel, sponsored by the Tor Project.
# most is copy-paste from intrigeri's tails gitlab
# scripts: https://gitlab.tails.boum.org/tails/jenkins-tools/-/blob/master/slaves/lint_po
# this script is published under the GPL v3 license.
# requires debian packages: python3-polib, gettext
'''
This script checks a directory with .po files for errors with links.
Translation errors with links are the most common errors on the
Tor Project websites.

Usage:

./check_markdown_links.py ../translation/

'''
import sys
import os

try:
    import polib
except ImportError:
    sys.exit("You need to install python3-polib to use this program.")

try:
    FOLDER = sys.argv[1]
except IndexError:
    FOLDER = os.getcwd()



def check_links(pofile):
    'lets check the links in one po file'
    results_in_markdown = '\n'
    broken_link_message = '\n### '+ ' '.join(pofile.metadata['Language-Team'].split(' ')[0:-1])
    broken_links = []
    for entry in pofile.translated_entries():
        if entry.msgid.count("](") != entry.msgstr.count("]("):
            broken_links.append(entry.msgstr)

    if broken_links != []:
        print('Broken links in '+' '.join(pofile.metadata['Language-Team'].split(' ')[0:-1]))
        results_in_markdown += broken_link_message
        results_in_markdown += '\n* '
        results_in_markdown += '\n* '.join(broken_links)
        print(broken_links)
    return results_in_markdown


def go_trough_files():
    'open each .po file and check for broken links'
    for f in os.listdir(FOLDER):
        if f.endswith('.po'):
            'we only want to check the .po files on the folder'
            po = polib.pofile(FOLDER+'/'+f, encoding='UTF-8')
            file_results = check_links(po)
            if file_results != '\n':
                with open("../broken_links.md", "a") as output_file:
                    output_file.write(file_results)
        else:
            'probably some other file on the repo'
            pass

def main():
    'we walk the files checking for broken links and we write the results to file'
    go_trough_files()

main()
