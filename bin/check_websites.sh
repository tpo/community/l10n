#!/bin/bash
# this script goes through the different lektor websites and performs
# tests on them, while generating a report in markdown
# once finished, it makes the markdown report onto HTML
# and publishes it into a gitlab page
# written by emmapeel, sponsored by the Tor Project.
# this script is published under the GPL v3 license.

WEBSITE_BRANCHES='tbmanual-contentspot communitytpo-contentspot tpo-web gettor-website-contentspot support-portal'

REPO_LOCATION='translation/'

printf "<html>\n<head>\n<title=\"translation QA scripts\">\n</head>\n<body>\n" > problems.md;
printf "# Problematic strings in lektor websites - $(date)" >> problems.md;
cd $REPO_LOCATION;
git fetch origin;
git config pull.rebase true
for branch in $WEBSITE_BRANCHES; do
    printf '\n\n## [%s](https://www.transifex.com/otf/tor-project-support-community-portal/%s)' $branch $branch >> ../problems.md;
    git checkout $branch ;
    git clean -f >&2 ;
    git pull;
    python3 ../bin/check_common_errors.py;
    done

printf "\n</body>\n</html>" >> problems.md;
cd - &>/dev/null;
rm -rf public && mkdir public;
markdown problems.md > public/index.html;
