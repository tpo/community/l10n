#!/usr/bin/python3

'''
This script adjusts the Lektor project to build with incomplete, unofficial
translations. The translations must still meet a minimum completeness threshold.
'''

import configparser
import os
import logging
import sys

POTFILE = 'i18n/src/contents.pot'
MIN_PERCENT_TRANSLATED = 15
LANGUAGES = {
 'am': {'direction': 'text-right',
        'language': 'አማርኛ (am)',
        'order': 'order-last',
        'url': '/am/'},
 'ar': {'direction': 'text-right',
        'language': 'عربية (ar)',
        'order': 'order-last',
        'url': '/ar/'},
 'az': {'direction': 'text-right',
        'language': 'Azərbaycan (az)',
        'order': 'order-last',
        'url': '/az/'},
 'ba': {'direction': 'text-left',
        'language': 'Башҡортса (ba)',
        'order': 'order-last',
        'url': '/ba/'},
 'be': {'direction': 'text-left',
        'language': 'беларуская (be)',
        'order': 'order-last',
        'url': '/be/'},
 'bg': {'direction': 'text-left',
        'language': 'Български (bg)',
        'order': 'order-last',
        'url': '/bg/'},
 'bn': {'direction': 'text-left',
        'language': 'বাংলা ভাষা (bn)',
        'order': 'order-last',
        'url': '/bn/'},
 'bs': {'direction': 'text-left',
        'language': 'bosanski (bs)',
        'order': 'order-last',
        'url': '/bs/'},
 'ca': {'direction': 'text-left',
        'language': 'Català (ca)',
        'order': 'order-last',
        'url': '/ca/'},
 'ce': {'direction': 'text-left',
        'language': 'Нохчийн мотт (ce)',
        'order': 'order-last',
        'url': '/ce/'},
 'crh': {'direction': 'text-left',
        'language': 'къырымтатар тили (crh)',
        'order': 'order-last',
        'url': '/crh/'},
 'cs': {'direction': 'text-left',
        'language': 'česky (cs)',
        'order': 'order-last',
        'url': '/cs/'},
 'da': {'direction': 'text-left',
        'language': 'Dansk (da)',
        'order': 'order-last',
        'url': '/da/'},
 'de': {'direction': 'text-left',
        'language': 'Deutsch (de)',
        'order': 'order-last',
        'url': '/de/'},
 'el': {'direction': 'text-left',
        'language': 'Ελληνικά (el)',
        'order': 'order-last',
        'url': '/el/'},
 'en': {'direction': 'text-left',
        'language': 'English (en)',
        'order': 'order-last',
        'url': '/'},
 'es': {'direction': 'text-left',
        'language': 'Español (es)',
        'order': 'order-last',
        'url': '/es/'},
 'es-AR': {'direction': 'text-left',
           'language': 'Español (AR)',
           'order': 'order-last',
           'url': '/es-AR/'},
 'et': {'direction': 'text-left',
        'language': 'Eesti (et)',
        'order': 'order-last',
        'url': '/et/'},
 'fa': {'direction': 'text-right',
        'language': 'فارسی (fa)',
        'order': 'order-last',
        'url': '/fa/'},
 'fa-AF': {'direction': 'text-right',
        'language': '(fa-AF) ﺩﺭی',
        'order': 'order-last',
        'url': '/fa-AF/'},
 'fi': {'direction': 'text-left',
        'language': 'Suomi (fi)',
        'order': 'order-last',
        'url': '/fi/'},
 'fr': {'direction': 'text-left',
        'language': 'Français (fr)',
        'order': 'order-last',
        'url': '/fr/'},
 'ga': {'direction': 'text-left',
        'language': 'Gaeilge (ga)',
        'order': 'order-last',
        'url': '/ga/'},
 'ha': {'direction': 'text-left',
        'language': 'Hausa (ha)',
        'order': 'order-last',
        'url': '/ha/'},
 'he': {'direction': 'text-right',
        'language': 'עברית (he)',
        'order': 'order-last',
        'url': '/he/'},
 'hi': {'direction': 'text-left',
        'language': 'हिन्दी (hi)',
        'order': 'order-last',
        'url': '/hi/'},
 'hr': {'direction': 'text-left',
        'language': 'hrvatski (hr)',
        'order': 'order-last',
        'url': '/hr/'},
 'ht': {'direction': 'text-left',
        'language': 'kreyòl (ht)',
        'order': 'order-last',
        'url': '/ha/'},
 'hu': {'direction': 'text-left',
        'language': 'Magyar nyelv (hu)',
        'order': 'order-last',
        'url': '/hu/'},
 'hy': {'direction': 'text-left',
        'language': 'Հայերեն (hy)',
        'order': 'order-last',
        'url': '/hy/'},
 'id': {'direction': 'text-left',
        'language': 'bahasa Indonesia (id)',
        'order': 'order-last',
        'url': '/id/'},
 'is': {'direction': 'text-left',
        'language': 'Íslenska (is)',
        'order': 'order-last',
        'url': '/is/'},
 'it': {'direction': 'text-left',
        'language': 'Italiano (it)',
        'order': 'order-last',
        'url': '/it/'},
 'ja': {'direction': 'text-left',
        'language': '日本語 (ja)',
        'order': 'order-last',
        'url': '/ja/'},
 'ka': {'direction': 'text-left',
        'language': 'ქართული ენა (ka)',
        'order': 'order-last',
        'url': '/ka/'},
 'kk': {'direction': 'text-left',
        'language': 'Kazakh (kk)',
        'order': 'order-last',
        'url': '/kk/'},
 'km': {'direction': 'text-left',
        'language': 'ភាសាខ្មែរ  (km)',
        'order': 'order-last',
        'url': '/km/'},
 'ko': {'direction': 'text-left',
        'language': '한국어 (ko)',
        'order': 'order-last',
        'url': '/ko/'},
 'lt': {'direction': 'text-left',
        'language': 'lietuvių (lt)',
        'order': 'order-last',
        'url': '/lt/'},
 'mk': {'direction': 'text-left',
        'language': 'македонски (mk)',
        'order': 'order-last',
        'url': '/mk/'},
 'ml': {'direction': 'text-left',
        'language': 'മലയാളം  (ml)',
        'order': 'order-last',
        'url': '/ml/'},
 'ms': {'direction': 'text-left',
        'language': 'Melayu (ms)',
        'order': 'order-last',
        'url': '/ms/'},
 'my': {'direction': 'text-left',
        'language': 'မြန်မာဘာသာ (my)',
        'order': 'order-last',
        'url': '/my/'},
 'nb': {'direction': 'text-left',
        'language': 'Norsk bokmål (nb)',
        'order': 'order-last',
        'url': '/nb/'},
 'nb-NO': {'direction': 'text-left',
           'language': 'Norsk bokmål (nb-NO)',
           'order': 'order-last',
           'url': '/nb-NO/'},
 'nl': {'direction': 'text-left',
        'language': 'Nederlands (nl)',
        'order': 'order-last',
        'url': '/nl/'},
 'pl': {'direction': 'text-left',
        'language': 'polski (pl)',
        'order': 'order-last',
        'url': '/pl/'},
 'ps': {'direction': 'text-right',
        'language': 'ﭖښﺕﻭ (ps)',
        'order': 'order-last',
        'url': '/ps/'},
 'pt-BR': {'direction': 'text-left',
           'language': 'Português Br. (pt-BR)',
           'order': 'order-last',
           'url': '/pt-BR/'},
 'pt-PT': {'direction': 'text-left',
           'language': 'Português Port. (pt-PT)',
           'order': 'order-last',
           'url': '/pt-PT/'},
 'ro': {'direction': 'text-left',
        'language': 'Română (ro)',
        'order': 'order-last',
        'url': '/ro/'},
 'ru': {'direction': 'text-left',
        'language': 'Русский (ru)',
        'order': 'order-last',
        'url': '/ru/'},
 'si': {'direction': 'text-left',
        'language': 'සිංහල (si)',
        'order': 'order-last',
        'url': '/si/'},
  'sk': {'direction': 'text-left',
        'language': 'slovenčina (sk)',
        'order': 'order-last',
        'url': '/sk/'},
 'sq': {'direction': 'text-left',
        'language': 'Shqip (sq)',
        'order': 'order-last',
        'url': '/sq/'},
 'sr': {'direction': 'text-left',
        'language': 'српски језик (sr)',
        'order': 'order-last',
        'url': '/sr/'},
 'sv': {'direction': 'text-left',
        'language': 'Svenska (sv)',
        'order': 'order-last',
        'url': '/sv/'},
 'sv-SE': {'direction': 'text-left',
           'language': 'Svenska (sv)',
           'order': 'order-last',
           'url': '/sv-SE/'},
 'sw': {'direction': 'text-left',
        'language': 'Kiswahili (sw)',
        'order': 'order-last',
        'url': '/sw/'},
 'ta': {'direction': 'text-left',
        'language': 'தமிழ்(ta)',
        'order': 'order-last',
        'url': '/ta/'},
 'th': {'direction': 'text-left',
        'language': 'ภาษาไทย (th)',
        'order': 'order-last',
        'url': '/th/'},
 'tk': {'direction': 'text-left',
        'language': 'Turkmen (tk)',
        'order': 'order-last',
        'url': '/tk/'},
 'tl': {'direction': 'text-left',
        'language': 'Tagalog (tl)',
        'order': 'order-last',
        'url': '/tl/'},
 'tr': {'direction': 'text-left',
        'language': 'Türkçe (tr)',
        'order': 'order-last',
        'url': '/tr/'},
 'uk': {'direction': 'text-left',
        'language': 'українська (uk)',
        'order': 'order-last',
        'url': '/uk/'},
 'vi': {'direction': 'text-left',
        'language': 'Tiếng Việt (vi)',
        'order': 'order-last',
        'url': '/vi/'},
 'wo': {'direction': 'text-left',
        'language': 'Wolof (wo)',
        'order': 'order-last',
        'url': '/wo/'},
 'zh-CN': {'direction': 'text-left',
           'language': '简体中文 (zh-CN)',
           'order': 'order-last',
           'url': '/zh-CN/'},
 'zh-TW': {'direction': 'text-left',
           'language': '正體中文 (zh-TW)',
           'order': 'order-last',
           'url': '/zh-TW/'}
}

# set up logging
# enable debug if L10N_STAGING_DEBUG envvar is set
logging.basicConfig(format="%(message)s")
if os.environ.get('L10N_STAGING_DEBUG'):
    logging.getLogger().setLevel(logging.DEBUG)

try:
    import polib
except ImportError:
    logging.error("You need to install python3-polib to use this program.")
    sys.exit(1)

# ensure i18n repo is checked out
if not os.path.exists(POTFILE):
    logging.error("Error: POT file %s does not exist, missing translation repository?", POTFILE)
    sys.exit(1)

# get project name from envvar, or from *.lektorproject file
project_name = os.environ.get('CI_PROJECT_NAME')
if not project_name:
    project_files = [x for x in os.listdir('.') if x.endswith('.lektorproject')]
    if not project_files:
        logging.error("Error: could not determine project name!")
        sys.exit(1)
    elif len(project_files) > 1:
        logging.error("Error: found multiple lektorproject files!")
        sys.exit(1)
    else:
        project_name = project_files[0][:-14]
logging.info("Got project name: %s", project_name)

# read lektorproject config
lektorproject_file = f'{project_name}.lektorproject'
if not os.path.exists(lektorproject_file):
    logging.error("Error: unable to find %s!", lektorproject_file)
    sys.exit(1)
lektorproject = configparser.ConfigParser()
lektorproject.read(lektorproject_file)

# read i18n plugin config
# we need to add a fake [MAIN] section to please ConfigParser
with open('configs/i18n.ini') as f:
    i18n_ini = '[MAIN]\n' + f.read()
i18nconfig = configparser.ConfigParser()
i18nconfig.read_string(i18n_ini)
enabled_langs = i18nconfig['MAIN']['translations'].split(',')
logging.info("Enabled languages: %s", ', '.join(enabled_langs))

# create empty dict for alternatives because we create it from scratch
# following the order in alternatives_all, if not, the languages
# will appear out of order in the drop-down
alternatives_new = {}

for lang in LANGUAGES:

    # po filename containing latest translations
    pofile = f'i18n/contents+{lang}.po'

    # proceed only if the language has a translation
    if os.path.exists(pofile) or lang == 'en':

        if lang == 'en':
            logging.info("Source language (en) enabled")
            alternatives_new[lang] = LANGUAGES[lang]
            continue

        if lang in enabled_langs:
            logging.info("Language %s is already enabled, skipping.", lang)
            alternatives_new[lang] = LANGUAGES[lang]
            continue

        po = polib.pofile(pofile)
        popct = po.percent_translated()

        if popct >= MIN_PERCENT_TRANSLATED:

            logging.info("Enabling %s (%d%% translated)", lang, popct)

            alternatives_new[lang] = LANGUAGES[lang]

            lektorproject[f'alternatives.{lang}'] = {
                'name': LANGUAGES[lang]['language'],
                'url_prefix': LANGUAGES[lang]['url'],
                'locale': lang
            }

            i18nconfig['MAIN']['translations'] += f',{lang}'

        else:
            logging.info("Not enabling %s (%d%% translated)", lang, popct)

# write updated lektorproject
with open(lektorproject_file, 'w') as configfile:
    lektorproject.write(configfile)

# debug lektorproject file
with open(lektorproject_file, 'r') as configfile:
    logging.debug(configfile.read())

# write updated alternatives databag
alternatives = configparser.ConfigParser()
alternatives.read_dict(alternatives_new)
with open('databags/alternatives.ini', 'w') as configfile:
    alternatives.write(configfile)

# write updated i18n config
with open('configs/i18n.ini', 'w') as f:
    for key, value in i18nconfig['MAIN'].items():
        f.write(f'{key} = {value}\n')
