#!/usr/bin/env python3
# written by emmapeel, for the Tor Project.
# this script is published under the GPL v3 license.
'''
This script generates a version of a given lektor website ($PROJECT)
with more languages enabled. It is meant to be triggered when there are changes
on the translations.git repo branch of a particular lektor website.
The projects called need to be configured at ./configs/lektors.ini

Usage:

./lektor_morelocales_build.py [PROJECT]

Where branchname is the name of the translation branch and transifex component

'''
import sys
import os
import logging
import configparser
import subprocess
try:
    import polib
except ImportError:
    sys.exit("You need to install python3-polib to use this program.")

try:
    PROJECT = sys.argv[1]
except IndexError:
    sys.exit("Please run me with one of the projects listed in ./configs/lektors.ini\ni.e. `./lektor_morelocales_build.py support` ")


lektor_config = configparser.ConfigParser()
lektor_config.read('./configs/lektors.ini')


class WebsiteProject:
    "the website we will build"
    def __init__(self, PROJECT):
        self.config = lektor_config[PROJECT]


project = WebsiteProject(PROJECT)

def prepare_repo(project):
    #TODO download lektor repo
    subprocess.run(["git", "clone", "--depth=1", project.config['repository'], PROJECT])
    #TODO download translations repo branch
    os.chdir(PROJECT)
    subprocess.run(["git", "submodule", "init"])
    subprocess.run(["git", "submodule", "update"])
    subprocess.run(["rm", "-rf", "i18n"])
    subprocess.run(["git", "clone", "--depth=1", "--branch", project.config['branch'], "https://git.torproject.org/translation.git", "i18n"])
    subprocess.run(["cp", f"../configs/{PROJECT}/alternatives.ini", "databags"])
    subprocess.run(["cp", f"../configs/{PROJECT}/{ project.config['lektorproject'] }", "."])
    subprocess.run(["git", "status"])
    #os.chdir('../')
    #TODO copy special config files

def build():
    print('#TODO lektor build on a subdir in tpo/community/l10n/ pages')


prepare_repo(project)

