#!/usr/bin/env python3

'''
This script evaluates a tmx file, and removes the items where the translation
is the same as the source string.

We have lots of those entries in our translation memory files, which makes it
slow and gives bad suggestions when translating. Cleaning up such files makes
translation easier and the results are more consistent.


Written by emmapeel, sponsored by the Tor Project.
this script is published under the GPL v3 license.
The part about gitlab alerts was stolen from lavamind

Usage:

./bin/clean-tmx.py file.tmx

'''
import sys
import logging
import xmltodict





# on this list we will save the good translations


# first we open the file

try:
    TMX_FILENAME = sys.argv[1]

except IndexError:
    print('Error: I need a tmx file!')
    logging.warning("Please run me with a tmx file, i.e. clean-tmx.py example.tmx")
    sys.exit()


with open(TMX_FILENAME,"r+") as tmxfile:
    goodlist = []
    xml_string=tmxfile.read()
    python_dict=xmltodict.parse(xml_string)

# here we pick the list with the actual terms in the dict
    words = python_dict['tmx']['body']['tu']

#now we go through them, and pick only the ones where the translation is
#different to the source

    print('Remove entries where the translation is equal to the source')
    for tmx in words:
        if tmx['tuv'][0]['seg'] == tmx['tuv'][1]['seg']:
            print('same as source')
        else:
            goodlist.append(tmx)

    print(f'Before: { len(words) }\nNow: { len(goodlist) }')

# now we will see if there are any duplicated memories

    print('Remove duplicated entries')

    unique_goodlist = []

    for tmem in goodlist:
        if tmem not in unique_goodlist:
            unique_goodlist.append(tmem)


    print(f'Before: { len(goodlist) }\nNow: { len(unique_goodlist) }')

# now we replace the bad list with the clean one in the dict

    python_dict['tmx']['body']['tu'] = unique_goodlist


# and rewrite the tmx file
    tmxfile.seek(0)
    tmxfile.truncate()
    xmltodict.unparse(python_dict, tmxfile, pretty=True)

    tmxfile.close()
