---
include:
  - project: tpo/tpa/ci-templates
    file: scripts/apt.yml

image: containers.torproject.org/tpo/tpa/base-images/python:bookworm

stages:
  - build

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  stage: build
  interruptible: true
  cache:
    key: $CI_PROJECT_PATH_SLUG
    paths:
      - .cache/apt
      - 'stats/*.json'
      - translation
      - 'public/pies'
      - '**/i18n/*'
  before_script:
    - !reference [.apt-init]
    - apt-get install -y
      i18nspector
      gettext
      git
      markdown
      python3-httpx
      python3-inifile
      python3-markdown
      python3-matplotlib
      python3-numpy
      python3-pandas
      python3-polib
      python3-requests
  script:
    - |-
      echo -e "\e[0Ksection_start:`date +%s`:translation_checkout\r\e[0KChecking out translations"
      if [ -d "translation" ]; then
        echo "the repo is already here"
        cd translation && git fetch origin && git config pull.rebase true && cd ../
      else
        echo "we need to clone the repo"
        git clone https://gitlab.torproject.org/tpo/translation.git
      fi
      echo -e "\e[0Ksection_end:`date +%s`:translation_checkout\r\e[0K"
    - |-
      echo -e "\e[0Ksection_start:`date +%s`:clean_cache\r\e[0KClean the cache for statistics"
      ifStart=`date '+%A'`
      if [ $ifStart == 'Sunday' ]
      then
        echo "This script should only run on Sundays."
        echo "checking for components history and events"
        echo "Now we remove the last pages, so next time the script runs they are retrieved again."
        echo "The previous pages are not supposed to change much, so they can be cached for more time."
        sh bin/clean_cached_jsons.sh
      else
        echo "Not running script, not a Sunday."
      fi
      echo -e "\e[0Ksection_end:`date +%s`:clean_cache\r\e[0K"
    - |-
      echo -e "\e[0Ksection_start:`date +%s`:check_branches\r\e[0KCheck all translation branches"
      if [ -d "public/*.html" ]; then
        rm public/*.html
        rm public/style.css
      fi
      bin/check_websites.sh
      echo -e "\e[0Ksection_end:`date +%s`:check_branches\r\e[0K"
    - |-
      echo -e "\e[0Ksection_start:`date +%s`:tb_stats\r\e[0KMaking stats for Tor Browser"
      if [ ! -d "stats" ]; then
        mkdir stats
      else
        rm -f stats/projects-comp-.json
        rm -f stats/projects-languages-.json
      fi
      python3 bin/check-torbrowser-translations.py $WEBLATE_TOKEN
      echo -e "\e[0Ksection_end:`date +%s`:tb_stats\r\e[0K"
    - |-
      echo -e "\e[0Ksection_start:`date +%s`:all_lang_stats\r\e[0KMaking stats for all languages"
      python3 bin/status-per-language.py $WEBLATE_TOKEN
      echo -e "\e[0Ksection_end:`date +%s`:all_lang_stats\r\e[0K"
    - cat ci-templates/stats-header.html stats/lang-stats.html ci-templates/stats-footer.html > public/stats.html
    - cp ci-templates/style.css public/
    - cp ci-templates/favicon.png public/
    - |-
      echo -e "\e[0Ksection_start:`date +%s`:check_template_updates\r\e[0KChecking for translation template updates in URLs"
      python3 bin/check-remote-updates.py
      echo -e "\e[0Ksection_end:`date +%s`:check_template_updates\r\e[0K"
    - |-
      echo -e "\e[0Ksection_start:`date +%s`:maintainer_stats\r\e[0KMaking maintainer stats"
      python3 bin/component-activity.py $WEBLATE_TOKEN
      echo "remove the last page of some cached json files to retrieve again, in case there are updates"
      echo -e "\e[0Ksection_end:`date +%s`:maintainer_stats\r\e[0K"

  artifacts:
    paths:
    - public/*
    - stats/*.md
